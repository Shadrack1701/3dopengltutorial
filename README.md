# README #

### This follows the  [Thin Matrix OpenGL Tutorials](https://www.youtube.com/playlist?list=PLRIWtICgwaX0u7Rf9zkZhLoLuZVfUksDP)###
* Was a learning project based on a YouTube tutorial to become familiar with OpenGL/AL and LWJGL.
* Version 0.01

### How do I get set up? ###
* Clone the project using source tree or download directly and use a Java IDE, I recomment Eclipse or IntelliJ
* Use Java 1.8 JDK and JRE
* In run configurations add VM Options: "-Djava.library.path=libs"
* If not detected automatically, add the following jars as compiled dependencies to your project structure:
    > lwjgl.jar
    > lwjgl_util.jar
    > slick-util.jar
    > PNGDecoder.jar