package com.trx.main.entities;

import com.trx.main.render.model.TexturedModel;
import com.trx.main.render.terrain.Terrain;
import com.trx.main.utils.Constants;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

public class Player extends Entity{
    private static final float RUN_SPEED = .5f;
    private static final float TURN_SPEED = 2f;
    private static final float JUMP_POWER = 1.2f;

    private static final float height = 4;
    private boolean inAir = false;
    private float currentSpeed = 0;
    private float currentTurnSpeed = 0;
    private float upwardsSpeed= 0;

    public Player(TexturedModel model, Vector3f position, Vector3f rotation, float scale) {
        super(model, position, rotation, scale);
    }

    public void move(Terrain terrain) {
        checkInputs();
        super.increaseRotation(new Vector3f(0, currentTurnSpeed, 0));
        float dx = (float) (currentSpeed * Math.sin(Math.toRadians(super.getRotation().getY())));
        float dz = (float) (currentSpeed * Math.cos(Math.toRadians(super.getRotation().getY())));
        upwardsSpeed += Constants.GRAVITY;
        float terrainHeight = terrain.getTerrainHeight(super.getPosition().x, super.getPosition().z);
        if ((super.getPosition().getY() + upwardsSpeed) < terrainHeight) {
            upwardsSpeed = 0;
            super.getPosition().setY(terrainHeight);
            inAir = false;
        }
        super.increasePosition(new Vector3f(dx, upwardsSpeed, dz));

    }

    private void checkInputs() {
        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            this.currentSpeed = RUN_SPEED;
        } else if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
            this.currentSpeed = -RUN_SPEED;
        } else this.currentSpeed = 0;

        if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            this.currentTurnSpeed = TURN_SPEED;
        } else if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            this.currentTurnSpeed = -TURN_SPEED;
        } else this.currentTurnSpeed = 0;

        if (Keyboard.isKeyDown(Keyboard.KEY_SPACE) && !inAir) {
            this.upwardsSpeed = JUMP_POWER;
            inAir = true;
        }
    }

    public static float getHeight() {
        return height;
    }
}
