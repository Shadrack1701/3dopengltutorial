package com.trx.main.entities;

import com.trx.main.render.terrain.Terrain;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

public class Camera {

    private float distanceFromPlayer = 50;
    private float angleAroundPlayer =  0;

	private Vector3f position = new Vector3f(0,0,0);
	private float pitch = 20;
	private float yaw ;
    private float roll;
    private boolean onGround = false;
	
	public Camera() {}

    public Camera(Camera camera){
        this.position = camera.getPosition();
        this.pitch = camera.getPitch();
        this.yaw = camera.getYaw();
        this.roll = camera.getRoll();
    }
	
	public void move(Player player, Terrain terrain){
        calculateZoom();
        calculateAnglePitch();
        float horizontalDistance = calculateHorizontalDistance();
        float verticalDistance = calculateVerticalDistance(player);
        yaw = 180 - (player.getRotation().getY() + angleAroundPlayer);
        calculateCameraPosition(horizontalDistance, verticalDistance, player, terrain);
	}

    private float calculateHorizontalDistance() {
        return (float) (distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
    }

    private float calculateVerticalDistance(Player player) {
        float vDistance = (float) (distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
        return vDistance;

    }

    private void calculateCameraPosition(float hDistance, float vDistance, Player player, Terrain terrain) {
        float theta = player.getRotation().getY() + angleAroundPlayer;
        float offSetX = (float) (hDistance * Math.sin(Math.toRadians(theta)));
        float offSetZ = (float) (hDistance * Math.cos(Math.toRadians(theta)));
        position.x = player.getPosition().x - offSetX;
        position.z = player.getPosition().z - offSetZ;
        position.y = player.getPosition().y + player.getHeight() + vDistance;
/*        if (position.y < terrain.getTerrainHeight(position.x, position.y)) {
            position.y = terrain.getTerrainHeight(position.x, position.y);
            onGround = true;
        }*/

    }

    private void calculateZoom() {
        float zoomLevel = Mouse.getDWheel() * 0.1f;
        distanceFromPlayer -= zoomLevel;
        if (distanceFromPlayer > 100) distanceFromPlayer = 100;
        if (distanceFromPlayer < 10) distanceFromPlayer = 10;
    }

    private void calculateAnglePitch() {
        if(Mouse.isButtonDown(1)) {
            float pitchChange = 0;
            float mousePitch = Mouse.getDY();
            //if ((onGround && mousePitch < 0) || !onGround) {
            pitchChange = mousePitch * 0.4f;
            //    onGround = false;
            //}
            float angleChange = Mouse.getDX() * 0.4f;
            angleAroundPlayer -= angleChange;
            pitch -= pitchChange;
        }
    }

    public void invertPitch() {
        this.pitch = -pitch;
    }

	public Vector3f getPosition() {
		return position;
	}

	public float getPitch() {
		return pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public float getRoll() {
		return roll;
	}
	
	

}
