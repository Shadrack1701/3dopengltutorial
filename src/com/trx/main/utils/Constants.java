package com.trx.main.utils;

public class Constants {

    public static final float RENDER_DISTANCE = 3600;
    public static final float GRAVITY = -.08f;
    public static int MAX_LIGHTS = 4;
    public static float FOV = 70;
    public static float NEAR_PLANE = 0.1f;
    public static float FAR_PLANE = 1000.0f;

}
