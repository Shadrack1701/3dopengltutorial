package com.trx.main;

import com.trx.main.controls.MousePicker;
import com.trx.main.entities.*;
import com.trx.main.render.*;
import com.trx.main.render.fonts.fontMeshCreator.FontType;
import com.trx.main.render.fonts.fontMeshCreator.GUIText;
import com.trx.main.render.fonts.fontRendering.TextMaster;
import com.trx.main.render.guis.GuiTexture;
import com.trx.main.render.model.RawModel;
import com.trx.main.render.model.TexturedModel;
import com.trx.main.render.particles.Particle;
import com.trx.main.render.particles.ParticleMaster;
import com.trx.main.render.particles.ParticleSystem;
import com.trx.main.render.particles.ParticleTexture;
import com.trx.main.render.skybox.SkyBoxRenderer;
import com.trx.main.render.texture.ModelTexture;
import com.trx.main.render.terrain.TerrainTexture;
import com.trx.main.render.terrain.TerrainTexturePack;
import com.trx.main.render.terrain.Terrain;
import com.trx.main.render.water.WaterFrameBuffers;
import com.trx.main.render.water.WaterRenderer;
import com.trx.main.render.water.WaterTile;
import com.trx.main.utils.Loader;
import com.trx.main.utils.Maths;
import com.trx.main.utils.OBJLoader;
import com.trx.main.utils.normalMappingObjConverter.NormalMappedObjLoader;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import java.io.File;
import java.util.*;

public class TraxGen {

    private static List<Terrain> terrains = new ArrayList<Terrain>();
    private static List<Entity> entities = new ArrayList<Entity>();
    private static List<Entity> normalEntities = new ArrayList<Entity>();
    private static List<Light> lights = new ArrayList<Light>();
    private static List<WaterTile> water = new ArrayList<WaterTile>();
    private static List<GuiTexture> guis = new ArrayList<GuiTexture>();
    private static WaterFrameBuffers fbos;
    private static Entity lampEntity;
    private static Light lampLight;
    private static Player player;
    private static MasterRenderer renderer;
    private static Entity barrel;
    private static Entity boulder;
    private static Entity crate;

    public static void  main (String[] args) {

        DisplayManager.createDisplay();
        Loader loader = new Loader();
        Camera camera = new Camera();
        MousePicker picker = new MousePicker(camera, terrains);
        fbos = new WaterFrameBuffers();
        renderer = new MasterRenderer(loader, fbos, camera);
        TextMaster.init(loader);
        createTestEntities(loader);

        ParticleTexture particleTexture = new ParticleTexture(loader.loadTexture("particleStar"), 1, true);
        ParticleTexture particleTexture2 = new ParticleTexture(loader.loadTexture("particleAtlas"), 4);
        ParticleSystem particleSystem = new ParticleSystem(100, 0, -0.01f, 100, 4, particleTexture2, 2000);
        particleSystem.randomizeRotation();
        particleSystem.setDirection(new Vector3f(0, 1, 0), 2.0f);
        particleSystem.setLifeError(1.0f);
        particleSystem.setSpeedError(0.5f);
        particleSystem.setScaleError(0.5f);

        ParticleSystem particleSystem2 = new ParticleSystem(200, .5f, 0.1f, 240, 1, particleTexture, 4000);
        particleSystem2.randomizeRotation();
        particleSystem2.setDirection(new Vector3f(0, 1, 0), 0.1f);
        particleSystem2.setLifeError(0.1f);
        particleSystem2.setSpeedError(0.4f);
        particleSystem2.setScaleError(0.8f);

        ParticleSystem particleSystem3 = new ParticleSystem(60, .5f, 0.1f, 240, 1, particleTexture, 1000);
        particleSystem3.randomizeRotation();
        particleSystem3.setDirection(new Vector3f(0, 1, 0), 0.1f);
        particleSystem3.setLifeError(0.1f);
        particleSystem3.setSpeedError(0.4f);
        particleSystem3.setScaleError(0.8f);

        ParticleSystem particleSystem4 = new ParticleSystem(60, .5f, 0.1f, 240, 1, particleTexture, 1000);
        particleSystem4.randomizeRotation();
        particleSystem4.setDirection(new Vector3f(0, 1, 0), 0.1f);
        particleSystem4.setLifeError(0.1f);
        particleSystem4.setSpeedError(0.4f);
        particleSystem4.setScaleError(0.8f);

        lights.add(SkyBoxRenderer.getSun());

        long lastTime = System.nanoTime();
        long lastTimer = System.currentTimeMillis();
        double nsPerTick = 1000000000D /60;
        double nsPerRender = 1000000000D /9999;
        double tDelta = 0, rDelta = 0;
        int ticks = 0;
        int frames = 0;
        while(!Display.isCloseRequested()) {
            long now = System.nanoTime();
            tDelta += (now - lastTime) / nsPerTick;
            rDelta += (now - lastTime) / nsPerRender;
            lastTime = now;

            while (tDelta >= 1) {
                player.move(Terrain.getTerrain(terrains, player.getPosition().x, player.getPosition().z));
                camera.move(player, Terrain.getTerrain(terrains, camera.getPosition().x, camera.getPosition().z));

                barrel.setRotation(new Vector3f(0, barrel.getRotation().getY() + .1f, 0));
                boulder.setRotation(new Vector3f(0, boulder.getRotation().getY() + .1f, 0));
                crate.setRotation(new Vector3f(0, crate.getRotation().getY() + .1f, 0));

                if (Mouse.isButtonDown(0)) {
                    picker.update();
                }
                if (picker.getCurrentTerrainPoint() != null) {
                    lampEntity.setPosition(picker.getCurrentTerrainPoint());
                    lampLight.setPosition(new Vector3f(picker.getCurrentTerrainPoint().x, picker.getCurrentTerrainPoint().y + 15, picker.getCurrentTerrainPoint().z));
                }
                SkyBoxRenderer.rotate();
                SkyBoxRenderer.dayNight();
                WaterRenderer.ripple();

                //particleSystem.generateParticles(new Vector3f(player.getPosition()));
                particleSystem2.generateParticles(new Vector3f(60, 6, 75));
                //particleSystem3.generateParticles(new Vector3f(68, 9, 75));
                //particleSystem4.generateParticles(new Vector3f(75, 7, 75));
                ParticleMaster.update(camera);

                ticks++;
                tDelta -= 1;
            }

            if (rDelta >= 1) {
                frames++;

                renderer.renderShadowMap(entities, camera);

                GL11.glEnable(GL30.GL_CLIP_DISTANCE0);
                fbos.bindReflectionFrameBuffer();
                float distance = 2 * (camera.getPosition().getY() - water.get(0).getPosition().getY());
                camera.getPosition().y -= distance;
                camera.invertPitch();
                renderer.preRenderScene(entities, normalEntities, terrains, lights, camera, new Vector4f(0, 1, 0, -water.get(0).getPosition().getY() + .1f), true);
                camera.getPosition().y += distance;
                camera.invertPitch();

                fbos.bindRefractionFrameBuffer();
                renderer.preRenderScene(entities, normalEntities, terrains, lights, camera, new Vector4f(0, -1, 0, water.get(0).getPosition().getY()), false);
                fbos.unbindCurrentFramebuffer();
                GL11.glDisable(GL30.GL_CLIP_DISTANCE0);

                renderer.renderScene(entities, normalEntities, terrains, lights, guis, water, camera, new Vector4f(0, 1, 0, 10000));
                TextMaster.render();
                DisplayManager.updateDisplay();
                rDelta -= 1;
            }

            if (System.currentTimeMillis() - lastTimer >= 10000) {
                lastTimer += 10000;
                System.out.println(ticks / 10 + " ticks , " + frames / 10 + " frames per second");
                //System.out.println("blend: " + SkyBoxRenderer.getBlendFactor() + ", dayCount: " + SkyBoxRenderer.getDayCounter() + ", day?" + SkyBoxRenderer.isDay() + ", skyColor: " + SkyBoxRenderer.getSkyColor());
                frames = 0;
                ticks = 0;
            }

        }
        TextMaster.cleanUp();
        renderer.cleanup();
        fbos.cleanUp();
        loader.cleanUp();
        DisplayManager.closeDisplay();

    }

    public static void createTestEntities(Loader loader) {
        TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("grass"));
        TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("mud"));
        TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("grassFlowers"));
        TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("path"));

        TerrainTexturePack texturePack = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
        TerrainTexture blendMap = new TerrainTexture(loader.loadMapTexture("blendMap"));
        terrains.add(new Terrain(0, 1, loader, texturePack, blendMap, "heightMap"));
        terrains.add(new Terrain(1, 1, loader, texturePack, blendMap, "heightMap"));
        terrains.add(new Terrain(0, 0, loader, texturePack, blendMap, "heightMap"));
        terrains.add(new Terrain(1, 0, loader, texturePack, blendMap, "heightMap"));

        RawModel lowPolyTreeMod = OBJLoader.loadObjModel("lowPolyTree", loader);
        TexturedModel lowPolyTreeMT = new TexturedModel(lowPolyTreeMod, new ModelTexture(loader.loadTexture("lowPolyTree")), 0.4f);
        lowPolyTreeMT.getTexture().setShineDamper(2);
        lowPolyTreeMT.getTexture().setReflectivity(.1f);

        RawModel treeMod = OBJLoader.loadObjModel("tree", loader);
        TexturedModel treeMT = new TexturedModel(treeMod,new ModelTexture(loader.loadTexture("tree")), 0.4f);
        treeMT.getTexture().setShineDamper(.1f);
        treeMT.getTexture().setReflectivity(.01f);

        RawModel pineMod = OBJLoader.loadObjModel("pine", loader);
        TexturedModel pineMT = new TexturedModel(pineMod,new ModelTexture(loader.loadTexture("pine")), 0.5f);
        pineMT.getTexture().setShineDamper(.1f);
        pineMT.getTexture().setReflectivity(.01f);

/*        RawModel grassMod = OBJLoader.loadObjModel("grassModel", loader);
        TexturedModel grassMT = new TexturedModel(grassMod,new ModelTexture(loader.loadTexture("grassTexture")), 0.01f);
        grassMT.getTexture().setShineDamper(2);
        grassMT.getTexture().setReflectivity(.1f);
        grassMT.getTexture().setHasTransparency(true);
        grassMT.getTexture().setUseFakeLighting(true);

        RawModel flowerMod = OBJLoader.loadObjModel("grassModel", loader);
        TexturedModel flowerMT = new TexturedModel(flowerMod,new ModelTexture(loader.loadTexture("flower")), 0.02f);
        flowerMT.getTexture().setShineDamper(2);
        flowerMT.getTexture().setReflectivity(.1f);
        flowerMT.getTexture().setHasTransparency(true);
        flowerMT.getTexture().setUseFakeLighting(true);*/

        RawModel fernMod = OBJLoader.loadObjModel("fern", loader);
        TexturedModel fernMT = new TexturedModel(fernMod,new ModelTexture(loader.loadTexture("fern")), 0.02f);
        fernMT.getTexture().setNumberOfRows(2);
        fernMT.getTexture().setShineDamper(2);
        fernMT.getTexture().setReflectivity(.1f);
        fernMT.getTexture().setHasTransparency(true);
        fernMT.getTexture().setUseFakeLighting(true);

        RawModel lampMod = OBJLoader.loadObjModel("lamp", loader);
        TexturedModel lampMT = new TexturedModel(lampMod, new ModelTexture(loader.loadTexture("lamp")), .2f);
        lampMT.getTexture().setShineDamper(10);
        lampMT.getTexture().setReflectivity(.6f);
        lampMT.getTexture().setUseFakeLighting(true);

        RawModel playerMod = OBJLoader.loadObjModel("person", loader);
        TexturedModel playerMT = new TexturedModel(playerMod, new ModelTexture(loader.loadTexture("playerTexture")), 1f);
        player = new Player(playerMT, new Vector3f(75, Terrain.getTerrain(terrains, 75, 75).getTerrainHeight(75, 75), 75), new Vector3f(0, 135, 0), .5f);

        entities.add(player);

        Random random = new Random();
        boolean add;
        for(int i = 0; i < 200 ; i++){
            add = true;
            Entity tempEntity = new Entity(lowPolyTreeMT, new Vector3f(random.nextFloat() * 1600, 0, random.nextFloat() * 1600),new Vector3f(0, random.nextFloat() * 360, 0), 0.8f);
            for (Entity checkEntity : entities) {
                if (Maths.Intersects(tempEntity.getPosition(), tempEntity.getModel().getSize(), checkEntity.getPosition(), checkEntity.getModel().getSize())) {
                    i--;
                    add = false;
                }
            }
            if (add) {
                tempEntity.getPosition().setY(Terrain.getTerrain(terrains, tempEntity.getPosition().getX(), tempEntity.getPosition().getZ()).getTerrainHeight(tempEntity.getPosition().getX(), tempEntity.getPosition().getZ()) - .1f);
                entities.add(tempEntity);
            }
        }

        for(int i = 0; i < 200 ; i++){
            add = true;
            Entity tempEntity = new Entity(treeMT, new Vector3f(random.nextFloat() * 1600, 0, random.nextFloat() * 1600),new Vector3f(0, random.nextFloat() * 360, 0), 4);
            for (Entity checkEntity : entities) {
                if (Maths.Intersects(tempEntity.getPosition(), tempEntity.getModel().getSize(), checkEntity.getPosition(), checkEntity.getModel().getSize())) {
                    i--;
                    add = false;
                }
            }
            if (add) {
                tempEntity.getPosition().setY(Terrain.getTerrain(terrains, tempEntity.getPosition().getX(), tempEntity.getPosition().getZ()).getTerrainHeight(tempEntity.getPosition().getX(), tempEntity.getPosition().getZ()) - .01f);
                entities.add(tempEntity);
            }
        }

        for(int i = 0; i < 200 ; i++){
            add = true;
            Entity tempEntity = new Entity(pineMT, new Vector3f(random.nextFloat() * 1600, 0, random.nextFloat() * 1600),new Vector3f(0, random.nextFloat() * 360, 0), 2);
            for (Entity checkEntity : entities) {
                if (Maths.Intersects(tempEntity.getPosition(), tempEntity.getModel().getSize(), checkEntity.getPosition(), checkEntity.getModel().getSize())) {
                    i--;
                    add = false;
                }
            }
            if (add) {
                tempEntity.getPosition().setY(Terrain.getTerrain(terrains, tempEntity.getPosition().getX(), tempEntity.getPosition().getZ()).getTerrainHeight(tempEntity.getPosition().getX(), tempEntity.getPosition().getZ()) - .01f);
                entities.add(tempEntity);
            }
        }

        for(int i = 0; i < 600 ; i++){
            add = true;
            Entity tempEntity = new Entity(fernMT, new Vector3f(random.nextFloat() * 1600, 0, random.nextFloat() * 1600),new Vector3f(0, random.nextFloat() * 360, 0), 0.7f, random.nextInt(4));
            for (Entity checkEntity : entities) {
                if (Maths.Intersects(tempEntity.getPosition(), tempEntity.getModel().getSize(), checkEntity.getPosition(), checkEntity.getModel().getSize())) {
                    i--;
                    add = false;
                }
            }
            if (add) {
                tempEntity.getPosition().setY(Terrain.getTerrain(terrains, tempEntity.getPosition().getX(), tempEntity.getPosition().getZ()).getTerrainHeight(tempEntity.getPosition().getX(), tempEntity.getPosition().getZ()) - .01f);
                entities.add(tempEntity);
            }
        }

/*        for(int i = 0; i < 1 ; i++){
            add = true;
            Entity tempEntity = new Entity(grassMT, new Vector3f(random.nextFloat() * 780, 0, random.nextFloat() * -780),new Vector3f(0, random.nextFloat() * 360, 0), 1);
            for (Entity checkEntity : entities) {
                if (Maths.Intersects(tempEntity.getPosition(), tempEntity.getModel().getSize(), checkEntity.getPosition(), checkEntity.getModel().getSize())) {
                    i--;
                    add = false;
                }
            }
            if (add) {
                tempEntity.getPosition().setY(terrain.getTerrainHeight(tempEntity.getPosition().getMaxX(), tempEntity.getPosition().getMaxZ()) - .01f);
                entities.add(tempEntity);
            }
        }

        for(int i = 0; i < 1 ; i++) {
            add = true;
            Entity tempEntity = new Entity(flowerMT, new Vector3f(random.nextFloat() * 780, 0, random.nextFloat() * -780), new Vector3f(0, random.nextFloat() * 360, 0), 1.0f);
            for (Entity checkEntity : entities) {
                if (Maths.Intersects(tempEntity.getPosition(), tempEntity.getModel().getSize(), checkEntity.getPosition(), checkEntity.getModel().getSize())) {
                    i--;
                    add = false;
                }
            }
            if (add) {
                tempEntity.getPosition().setY(terrain.getTerrainHeight(tempEntity.getPosition().getMaxX(), tempEntity.getPosition().getMaxZ()) - .01f);
                entities.add(tempEntity);
            }
        }*/

        guis.add(new GuiTexture(loader.loadTexture("health"), new Vector2f(-.7f, -.9f), new Vector2f(.3f, .4f)));
        float tempHeight;

        tempHeight = Terrain.getTerrain(terrains, 185, 293).getTerrainHeight(185, 293);
        lights.add(new Light(new Vector3f(185, tempHeight + 15, 293),new Vector3f(2, 0, 0), new Vector3f(1, 0.01f, 0.002f)));
        entities.add(new Entity(lampMT, new Vector3f(185,Terrain.getTerrain(terrains, 185, 293).getTerrainHeight(185, 293), 293), new Vector3f(0, 0, 0), 1));

        tempHeight = Terrain.getTerrain(terrains, 370, 300).getTerrainHeight(370, 300);
        lights.add(new Light(new Vector3f(370, tempHeight + 15, 300),new Vector3f(0, 2, 2), new Vector3f(1, 0.01f, 0.002f)));
        entities.add(new Entity(lampMT, new Vector3f(370, Terrain.getTerrain(terrains, 370, 300).getTerrainHeight(370, 300), 300), new Vector3f(0, 0, 0), 1));

        lampEntity = new Entity(lampMT, new Vector3f(0, -100, 0), new Vector3f(0, 0, 0), 1);
        entities.add(lampEntity);

        lampLight = new Light(new Vector3f(0, -100, 0),new Vector3f(2, 2, 2), new Vector3f(1, 0.01f, 0.002f));
        lights.add(lampLight);

        water.add(new WaterTile(new Vector3f(120, -1.5f, 120)));

        TexturedModel barrelModel = new TexturedModel(NormalMappedObjLoader.loadOBJ("barrel", loader), new ModelTexture(loader.loadTexture("barrel")), 1);
        barrelModel.getTexture().setNormalMap(loader.loadTexture("barrelNormal"));
        barrelModel.getTexture().setShineDamper(10);
        barrelModel.getTexture().setReflectivity(0.5f);

        TexturedModel boulderModel = new TexturedModel(NormalMappedObjLoader.loadOBJ("boulder", loader), new ModelTexture(loader.loadTexture("boulder")), 1);
        boulderModel.getTexture().setNormalMap(loader.loadTexture("boulderNormal"));
        boulderModel.getTexture().setShineDamper(5);
        boulderModel.getTexture().setReflectivity(0.2f);

        TexturedModel crateModel = new TexturedModel(NormalMappedObjLoader.loadOBJ("crate", loader), new ModelTexture(loader.loadTexture("crate")), 1);
        crateModel.getTexture().setNormalMap(loader.loadTexture("crateNormal"));
        crateModel.getTexture().setShineDamper(5);
        crateModel.getTexture().setReflectivity(0.3f);

        barrel = new Entity(barrelModel, new Vector3f(75, 5, 75), new Vector3f(0, 0, 0), 0.5f);
        boulder = new Entity(boulderModel, new Vector3f(68, 5, 75), new Vector3f(0, 0, 0), 0.5f);
        crate = new Entity(crateModel, new Vector3f(60, 5, 75), new Vector3f(0,0,0), .02f);

        normalEntities.add(barrel);
        normalEntities.add(boulder);
        normalEntities.add(crate);

        FontType font = new FontType(loader.loadFontTexture("candara"), new File("res/fonts/candara.fnt"));
        GUIText text = new GUIText("This is a test text!", 2, new Vector2f(0, 0), 0.5f, font, false);
        text.setFontColour(1, 0, 0);

        //guis.add(new GuiTexture(renderer.getShadowMapTexture(), new Vector2f(-0.5f, 0.5f), new Vector2f(0.5f, 0.5f)));

    }

}
