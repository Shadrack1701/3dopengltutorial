package com.trx.main.render.texture;

public class ModelTexture {

    private int textureID;
    private int normalMap;

    private float shineDamper = 1;
    private float reflectivity = 0;
    private boolean hasTransparency = false;
    private boolean useFakeLighting = false;

    private float numberOfRows = 1;

    public ModelTexture(int textureId) {
        this.textureID = textureId;
    }

    public int getTextureID() {
        return textureID;
    }
    public int getNormalMap() {
        return normalMap;
    }
    public void setNormalMap(int normalMap) {
        this.normalMap = normalMap;
    }
    public float getShineDamper() {
        return shineDamper;
    }
    public void setShineDamper(float shineDamper) {
        this.shineDamper = shineDamper;
    }
    public float getReflectivity() {
        return reflectivity;
    }
    public void setReflectivity(float reflectivity) {
        this.reflectivity = reflectivity;
    }
    public boolean isHasTransparency() {
        return hasTransparency;
    }
    public void setHasTransparency(boolean hasTransparency) {
        this.hasTransparency = hasTransparency;
    }
    public boolean isUseFakeLighting() {
        return useFakeLighting;
    }
    public void setUseFakeLighting(boolean useFakeLighting) {
        this.useFakeLighting = useFakeLighting;
    }
    public float getNumberOfRows() {
        return numberOfRows;
    }
    public void setNumberOfRows(float numberOfRows) {
        this.numberOfRows = numberOfRows;
    }
}
