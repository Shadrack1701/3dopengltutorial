package com.trx.main.render.particles;

import com.trx.main.entities.Camera;
import com.trx.main.utils.Loader;
import org.lwjgl.util.vector.Vector4f;

import java.util.*;

public class ParticleMaster {

    private static Map<ParticleTexture, List<Particle>> particles = new HashMap<ParticleTexture, List<Particle>>();
    private static ParticleRenderer renderer;

    public static void init(Loader loader) {
        renderer = new ParticleRenderer(loader, new ParticleShader());
    }

    public static void update(Camera camera) {
        Iterator<Map.Entry<ParticleTexture, List<Particle>>> mapIterator = particles.entrySet().iterator();
        while(mapIterator.hasNext()) {
            Map.Entry<ParticleTexture, List<Particle>> entry = mapIterator.next();
            List<Particle> list = entry.getValue();
            Iterator<Particle> iterator = list.iterator();
            while (iterator.hasNext()) {
                Particle p = iterator.next();
                if (!p.update(camera)) {
                    iterator.remove();
                    if(list.isEmpty()) mapIterator.remove();
                }
            }
            if (!entry.getKey().isAdditive()) {
                InsertionSort.sortHighToLow(list);
            }
        }
    }

    public static void render(Camera camera, Vector4f clipPlane, Boolean particleRender) {
        renderer.render(particles, camera, clipPlane, particleRender);
    }

    public static void cleanUp() {
        renderer.cleanUp();
    }

    public static void add(Particle particle) {
        List<Particle> list = particles.get(particle.getTexture());
        if (list == null) {
            list = new ArrayList<Particle>();
            particles.put(particle.getTexture(), list);
        }
        list.add(particle);
    }

    public static boolean isEmpty() {
       return particles.isEmpty();
    }
}
