package com.trx.main.render.particles;


import com.trx.main.entities.Camera;
import com.trx.main.utils.Constants;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class Particle {

    private Vector3f position;
    private Vector3f velocity;
    private float gravityEffect;
    private float lifeLength;
    private float rotation;
    private float scale;
    private float elapsedTime = 0;
    private float textureBlend;
    private Vector2f textureOffset1 = new Vector2f();
    private Vector2f textureOffset2 = new Vector2f();
    private ParticleTexture texture;
    private float distance;

    public Particle(){}

    public Particle(Vector3f position, Vector3f velocity, float gravityEffect, float lifeLength, float rotation, float scale, ParticleTexture texture) {
        this.position = position;
        this.velocity = velocity;
        this.gravityEffect = gravityEffect;
        this.lifeLength = lifeLength;
        this.rotation = rotation;
        this.scale = scale;
        this.texture = texture;
        ParticleMaster.add(this);
    }

    public void setActive(Vector3f position, Vector3f velocity, float gravityEffect, float lifeLength, float rotation, float scale, ParticleTexture texture) {
        this.position = position;
        this.velocity = velocity;
        this.gravityEffect = gravityEffect;
        this.lifeLength = lifeLength;
        this.rotation = rotation;
        this.scale = scale;
        this.texture = texture;
        elapsedTime = 0;
        ParticleMaster.add(this);
    }

    public boolean update(Camera camera) {
        elapsedTime++;
        if (elapsedTime < lifeLength) {
            velocity.y += Constants.GRAVITY * gravityEffect;
            Vector3f.add(velocity, position, position);
            distance = Vector3f.sub(camera.getPosition(), position, null).lengthSquared();
            updateTextureInfo();
            return true;
        } else {
            return false;
        }
    }

    private void updateTextureInfo() {
        float lifeFactor = elapsedTime / lifeLength;
        int stageCount = texture.getNumberOfRows() * texture.getNumberOfRows();
        float atlasProgressionn = lifeFactor * stageCount;
        int index1 = (int) Math.floor(atlasProgressionn);
        int index2 = index1 < stageCount - 1 ? index1 + 1: index1;
        this.textureBlend = atlasProgressionn % 1;
        setTextureOffsets(textureOffset1, index1);
        setTextureOffsets(textureOffset2, index2);
    }

    private void setTextureOffsets(Vector2f offset, int index) {
        int column = index % texture.getNumberOfRows();
        int row = index / texture.getNumberOfRows();
        offset.x = (float) column / texture.getNumberOfRows();
        offset.y = (float) row/ texture.getNumberOfRows();
    }

    protected Vector3f getPosition() {
        return position;
    }

    protected float getRotation() {
        return rotation;
    }

    protected float getScale() {
        return scale;
    }

    public ParticleTexture getTexture() {
        return texture;
    }

    public float getTextureBlend() {
        return textureBlend;
    }

    public Vector2f getTextureOffset2() {
        return textureOffset2;
    }

    public Vector2f getTextureOffset1() {
        return textureOffset1;
    }

    public float getDistance() {
        return distance;
    }
}
