package com.trx.main.render.water;

import org.lwjgl.util.vector.Vector3f;

public class WaterTile {

    public static final float TILE_SIZE = 100;

   Vector3f position;

    public WaterTile(Vector3f position) {
        this.position = position;
    }

    public Vector3f getPosition() {
        return position;
    }

}
