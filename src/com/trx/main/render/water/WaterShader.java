package com.trx.main.render.water;

import com.trx.main.entities.Camera;
import com.trx.main.entities.Light;
import com.trx.main.render.shader.ShaderProgram;
import com.trx.main.utils.Constants;
import com.trx.main.utils.Maths;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import java.util.List;

public class WaterShader extends ShaderProgram {

    private static final String VERTEX_FILE = "res/shaders/waterVertexShader.txt";
    private static final String FRAGMENT_FILE = "res/shaders/waterFragmentShader.txt";

    private int location_projectionMatrix;
    private int location_viewMatrix;
    private int location_modelMatrix;
    private int location_reflectionTexture;
    private int location_refractionTexture;
    private int location_dudvMap;
    private int location_normalMap;
    private int location_moveFactor;
    private int location_cameraPosition;
    private int location_lightPosition[];
    private int location_lightColour[];
    private int location_attenuation[];
    private int location_shineDamper;
    private int location_reflectivity;
    private int location_depthMap;
    private int location_nearPlane;
    private int location_farPlane;

    public WaterShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
    }

    @Override
    protected void getAllUniformLocations() {
        location_projectionMatrix = super.getUniformLocation("projectionMatrix");
        location_viewMatrix = super.getUniformLocation("viewMatrix");
        location_modelMatrix = super.getUniformLocation("modelMatrix");
        location_reflectionTexture = super.getUniformLocation("reflectionTexture");
        location_refractionTexture = super.getUniformLocation("refractionTexture");
        location_dudvMap = super.getUniformLocation("dudvMap");
        location_normalMap = super.getUniformLocation("normalMap");
        location_moveFactor = super.getUniformLocation("moveFactor");
        location_cameraPosition = super.getUniformLocation("cameraPosition");
        location_shineDamper = super.getUniformLocation("shineDamper");
        location_reflectivity = super.getUniformLocation("reflectivity");
        location_depthMap = super.getUniformLocation("depthMap");
        location_nearPlane = super.getUniformLocation("nearPlane");
        location_farPlane = super.getUniformLocation("farPlane");

        location_lightPosition = new int[Constants.MAX_LIGHTS];
        location_lightColour = new int[Constants.MAX_LIGHTS];
        location_attenuation = new int[Constants.MAX_LIGHTS];
        for (int i = 0; i < Constants.MAX_LIGHTS; i++) {
            location_lightPosition[i] = super.getUniformLocation("lightPosition[" + i + "]");
            location_lightColour[i] = super.getUniformLocation("lightColour[" + i + "]");
            location_attenuation[i] = super.getUniformLocation("attenuation[" + i + "]");
        }
    }

    public void loadPlaneValues(float near, float far) {
        super.loadFloat(location_nearPlane, near);
        super.loadFloat(location_farPlane, far);
    }

    public void loadShineVariables(float damper,float reflectivity){
        super.loadFloat(location_shineDamper, damper);
        super.loadFloat(location_reflectivity, reflectivity);
    }

    public void loadMoveFactor(float moveFactor) {
        super.loadFloat(location_moveFactor, moveFactor);
    }

    public void connectTextureUnits() {
        super.loadInt(location_reflectionTexture, 0);
        super.loadInt(location_refractionTexture, 1);
        super.loadInt(location_dudvMap, 2);
        super.loadInt(location_normalMap, 3);
        super.loadInt(location_depthMap, 4);
    }

    public void loadLight(List<Light> lights){
        for(int i = 0; i < Constants.MAX_LIGHTS; i++) {
            if (i < lights.size()) {
                super.loadVector(location_lightPosition[i], lights.get(i).getPosition());
                super.loadVector(location_lightColour[i], lights.get(i).getColour());
                super.loadVector(location_attenuation[i], lights.get(i).getAttenuation());
            } else {
                super.loadVector(location_lightPosition[i], new Vector3f(0, 0, 0));
                super.loadVector(location_lightColour[i], new Vector3f(0, 0, 0));
                super.loadVector(location_attenuation[i], new Vector3f(1, 0, 0));
            }
        }
    }

    public void loadModelMatrix(Matrix4f matrix){
        super.loadMatrix(location_modelMatrix, matrix);
    }

    public void loadViewMatrix(Camera camera){
        super.loadVector(location_cameraPosition, camera.getPosition());
        Matrix4f viewMatrix = Maths.createViewMatrix(camera);
        super.loadMatrix(location_viewMatrix, viewMatrix);
    }

    public void loadProjectionMatrix(Matrix4f projection){
        super.loadMatrix(location_projectionMatrix, projection);
    }

}
