package com.trx.main.render.water;

import com.trx.main.utils.Constants;
import com.trx.main.utils.Loader;
import com.trx.main.entities.Camera;
import com.trx.main.render.model.RawModel;
import com.trx.main.utils.Maths;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import java.util.List;

public class WaterRenderer {

    private static final String DUDV_MAP = "waterDUDV";
    private static final String NORMAL_MAP = "normalMap";
    private static final float WAVE_SPEED = 0.0005f;

    private RawModel quad;
    private WaterFrameBuffers fbos;
    private int dudvTexture;
    private int normalMap;

    private static float moveFactor = 0;

    public WaterRenderer(WaterShader shader, Loader loader, WaterFrameBuffers fbos){
        setUpVAO(loader);
        this.fbos = fbos;
        dudvTexture = loader.loadMapTexture(DUDV_MAP);
        normalMap = loader.loadMapTexture(NORMAL_MAP);
        shader.start();
        shader.connectTextureUnits();
        shader.loadProjectionMatrix(Maths.createProjectionMatrix());
        shader.loadPlaneValues(Constants.NEAR_PLANE, Constants.FAR_PLANE);
        shader.stop();
    }

    public void render(List<WaterTile> water, WaterShader shader) {
        prepareRender(shader);
        for(WaterTile tile : water) {
            Matrix4f modelMatrix = Maths.createTransformationMatrix(tile.getPosition(), new Vector3f(0,0,0), WaterTile.TILE_SIZE);
            shader.loadModelMatrix(modelMatrix);
            GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, quad.getVertexCount());
        }
        unbind();
    }

    private void prepareRender(WaterShader shader) {
        shader.loadMoveFactor(moveFactor);
        shader.loadShineVariables(20, 0.6f);
        GL30.glBindVertexArray(quad.getVaoID());
        GL20.glEnableVertexAttribArray(0);
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, fbos.getReflectionTexture());
        GL13.glActiveTexture(GL13.GL_TEXTURE1);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, fbos.getRefractionTexture());
        GL13.glActiveTexture(GL13.GL_TEXTURE2);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, dudvTexture);
        GL13.glActiveTexture(GL13.GL_TEXTURE3);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, normalMap);
        GL13.glActiveTexture(GL13.GL_TEXTURE4);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, fbos.getRefractionDepthTexture());

        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
    }

    private void unbind() {
        GL11.glDisable(GL11.GL_BLEND);
        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);
    }

    private void setUpVAO(Loader loader) {
        float[] verticies = {-1, -1, -1, 1, 1, -1, 1, -1, -1, 1, 1, 1};
        quad = loader.loadToVAO(verticies, 2);
    }

    public static void ripple() {
        moveFactor += WAVE_SPEED;
        moveFactor %= 1;
    }

}
