package com.trx.main.render.model;

import com.trx.main.render.texture.ModelTexture;

public class TexturedModel {
	
	private RawModel rawModel;
	private ModelTexture texture;
    private float size;

	
	public TexturedModel(RawModel model, ModelTexture texture, float size){
		this.rawModel = model;
		this.texture = texture;
        this.size = size;
	}

	public RawModel getRawModel() {
		return rawModel;
	}
	public ModelTexture getTexture() {
		return texture;
	}
    public float getSize() {
        return size;
    }
}
