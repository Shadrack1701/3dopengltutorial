package com.trx.main.render.fonts.fontRendering;;

import com.trx.main.render.fonts.fontMeshCreator.GUIText;
import com.trx.main.render.shader.ShaderProgram;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class FontShader extends ShaderProgram {

	private static final String VERTEX_FILE = "res/shaders/fontVertex.txt";
	private static final String FRAGMENT_FILE = "res/shaders/fontFragment.txt";

    private int location_translation;
	private int location_fontColour;
    private int location_fontWidth;
    private int location_fontEdge;
    private int location_borderWidth;
    private int location_borderEdge;
    private int location_borderColour;
    private int location_borderOffset;


	public FontShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void getAllUniformLocations() {
		location_translation = super.getUniformLocation("translation");
        location_fontColour = super.getUniformLocation("colour");
        location_fontWidth = super.getUniformLocation("fontWidth");
        location_fontEdge = super.getUniformLocation("fontEdge");
        location_borderWidth = super.getUniformLocation("borderWidth");
        location_borderEdge = super.getUniformLocation("borderEdge");
        location_borderColour = super.getUniformLocation("borderColour");
        location_borderOffset = super.getUniformLocation("borderOffset");
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
	}

    public void loadText(GUIText text) {
        loadTranslation(text.getPosition());
        loadFontColour(text.getFontColour());
        loadFontWidth(text.getFontWidth());
        loadFontEdge(text.getFontEdge());
        loadBorderWidth(text.getBorderWidth());
        loadBorderEdge(text.getBorderEdge());
        loadBorderColour(text.getBorderColour());
        loadBorderOffset(text.getBorderOffset());

    }

    protected void loadTranslation(Vector2f translation){
        super.loadVector(location_translation, translation);
    }

	protected void loadFontColour(Vector3f colour){
		super.loadVector(location_fontColour, colour);
	}

    protected void loadFontEdge(float edge) {
        super.loadFloat(location_fontEdge, edge);
    }

    protected void loadFontWidth(float fontWidth) {
        super.loadFloat(location_fontWidth, fontWidth);
    }

    protected void loadBorderWidth(float borderWidth) {
        super.loadFloat(location_borderWidth, borderWidth);
    }

    protected void loadBorderEdge(float borderEdge) {
        super.loadFloat(location_borderEdge, borderEdge);
    }

    protected void loadBorderColour(Vector3f borderColour) {
        super.loadVector(location_borderColour, borderColour);
    }

    protected void loadBorderOffset(Vector2f borderOffset) {
        super.loadVector(location_fontWidth, borderOffset);
    }

}
