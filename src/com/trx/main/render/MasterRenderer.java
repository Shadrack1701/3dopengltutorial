package com.trx.main.render;

import com.trx.main.render.guis.GuiTexture;
import com.trx.main.entities.EntityShader;
import com.trx.main.render.normalEntities.NormalMappingRenderer;
import com.trx.main.render.normalEntities.NormalMappingShader;
import com.trx.main.render.particles.*;
import com.trx.main.render.shadows.ShadowMapMasterRenderer;
import com.trx.main.render.skybox.SkyBoxShader;
import com.trx.main.render.skybox.SkyBoxRenderer;
import com.trx.main.render.water.WaterFrameBuffers;
import com.trx.main.render.water.WaterRenderer;
import com.trx.main.render.water.WaterShader;
import com.trx.main.render.water.WaterTile;
import com.trx.main.utils.Constants;
import com.trx.main.utils.Loader;
import com.trx.main.entities.Camera;
import com.trx.main.entities.Entity;
import com.trx.main.entities.EntityRenderer;
import com.trx.main.entities.Light;
import com.trx.main.render.guis.GuiRenderer;
import com.trx.main.render.model.TexturedModel;
import com.trx.main.render.guis.GuiShader;
import com.trx.main.render.terrain.TerrainShader;
import com.trx.main.render.terrain.TerrainRenderer;
import com.trx.main.render.terrain.Terrain;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MasterRenderer {

    private SkyBoxShader skyBoxShader = new SkyBoxShader();
    private WaterShader waterShader = new WaterShader();
    private EntityShader entityShader = new EntityShader();
    private TerrainShader terrainShader = new TerrainShader();
    private GuiShader guiShader = new GuiShader();
    private NormalMappingShader normalMappingShader = new NormalMappingShader();

    private SkyBoxRenderer skyBoxRenderer;
    private WaterRenderer waterRenderer;
    private EntityRenderer entityRenderer = new EntityRenderer(entityShader);
    private TerrainRenderer terrainRenderer = new TerrainRenderer(terrainShader);
    private GuiRenderer guiRenderer;
    private NormalMappingRenderer normalMappingRenderer = new NormalMappingRenderer(normalMappingShader);
    private ShadowMapMasterRenderer shadowMapMasterRenderer;

    private List<WaterTile> water = new ArrayList<WaterTile>();
    private Map<TexturedModel, List<Entity>> entities = new HashMap<TexturedModel, List<Entity>>();
    private Map<TexturedModel, List<Entity>> normalEntities = new HashMap<TexturedModel, List<Entity>>();
    private List<Terrain> terrains = new ArrayList<Terrain>();
    private List<GuiTexture> guis = new ArrayList<GuiTexture>();

    public MasterRenderer (Loader loader, WaterFrameBuffers fbos, Camera camera) {
        waterRenderer = new WaterRenderer(waterShader, loader, fbos);
        guiRenderer = new GuiRenderer(loader, guiShader);
        skyBoxRenderer = new SkyBoxRenderer(loader, skyBoxShader);
        this.shadowMapMasterRenderer = new ShadowMapMasterRenderer(camera);
        ParticleMaster.init(loader);
        enableCulling();
    }

    public void renderScene(List<Entity> entities, List<Entity> normalEntities, List<Terrain> terrains, List<Light> lights, List<GuiTexture> passedGuis, List<WaterTile> passedWater,  Camera camera, Vector4f clipPlane) {
        processWater(passedWater, camera);
        processEntity(entities, camera);
        processNormalEntity(normalEntities, camera);
        processGui(passedGuis);
        processTerrain(terrains);

        render(lights, camera, clipPlane, null);
    }

    public void preRenderScene(List<Entity> entities, List<Entity> normalEntities, List<Terrain> terrains, List<Light> lights, Camera camera, Vector4f clipPlane, Boolean particleRender) {
        processEntity(entities, camera);
        processNormalEntity(normalEntities, camera);
        processTerrain(terrains);

        render(lights, camera, clipPlane, particleRender);
    }

    public void render(List<Light> lights, Camera camera, Vector4f clipPlane, Boolean particleRender) {
        prepare();
        skyBoxShader.start();
        skyBoxShader.loadViewMatrix(camera);
        skyBoxShader.loadFogColor(SkyBoxRenderer.getSkyColor());
        skyBoxRenderer.render();
        skyBoxShader.stop();

        if (!water.isEmpty()) {
            waterShader.start();
            waterShader.loadViewMatrix(camera);
            waterShader.loadLight(lights);
            waterRenderer.render(water, waterShader);
            waterShader.stop();
            water.clear();
        }

        if (!entities.isEmpty()) {
            entityShader.start();
            entityShader.loadClipPlane(clipPlane);
            entityShader.loadSkyColor(SkyBoxRenderer.getSkyColor());
            entityShader.loadLight(lights);
            entityShader.loadViewMatrix(camera);
            entityRenderer.render(entities);
            entityShader.stop();
            entities.clear();
        }

        if (!normalEntities.isEmpty()) {
            normalMappingShader.start();
            normalMappingShader.loadClipPlane(clipPlane);
            normalMappingShader.loadSkyColor(SkyBoxRenderer.getSkyColor());
            normalMappingShader.loadLights(lights, camera);
            normalMappingRenderer.render(normalEntities);
            normalMappingShader.stop();
            normalEntities.clear();
        }

        if (!terrains.isEmpty()) {
            terrainShader.start();
            terrainShader.loadClipPlane(clipPlane);
            terrainShader.loadSkyColor(SkyBoxRenderer.getSkyColor());
            terrainShader.loadLight(lights);
            terrainShader.loadViewMatrix(camera);
            terrainRenderer.render(terrains, shadowMapMasterRenderer.getToShadowMapSpaceMatrix());
            terrainShader.stop();
            terrains.clear();
        }

        if (!ParticleMaster.isEmpty()) {
            ParticleMaster.render(camera, clipPlane, particleRender);
        }

        if (!guis.isEmpty()) {
            guiShader.start();
            guiRenderer.render(guis);
            guiShader.stop();
            guis.clear();
        }
    }

    public void prepare() {
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT|GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(SkyBoxRenderer.getSkyColor().x, SkyBoxRenderer.getSkyColor().y, SkyBoxRenderer.getSkyColor().z, 1);
    }

    public void processWater(List<WaterTile> passedWater, Camera camera) {
        for (WaterTile water: passedWater) {
            if (checkDistance(camera.getPosition(), water.getPosition())) this.water.add(water);
        }
    }

    public void processTerrain(List<Terrain> terrains) {
        this.terrains.addAll(terrains);
    }

    public void processEntity(List<Entity> passedEntities, Camera camera) {
        for (Entity entity : passedEntities) {
            if (!checkDistance(camera.getPosition(), entity.getPosition())) continue;
            TexturedModel texturedModel = entity.getModel();
            List<Entity> batch = entities.get(texturedModel);
            if (batch != null) {
                batch.add(entity);
            } else {
                List<Entity> newBatch = new ArrayList<Entity>();
                newBatch.add(entity);
                entities.put(texturedModel, newBatch);
            }
        }
    }

    public void processNormalEntity(List<Entity> passedEntities, Camera camera) {
        for (Entity entity : passedEntities) {
            if (!checkDistance(camera.getPosition(), entity.getPosition())) continue;
            TexturedModel texturedModel = entity.getModel();
            List<Entity> batch = normalEntities.get(texturedModel);
            if (batch != null) {
                batch.add(entity);
            } else {
                List<Entity> newBatch = new ArrayList<Entity>();
                newBatch.add(entity);
                normalEntities.put(texturedModel, newBatch);
            }
        }
    }

    public void processGui(List<GuiTexture> passedGuis) {
        this.guis.addAll(passedGuis);
    }

    public void renderShadowMap(List<Entity> entityList, Camera camera) {
        processEntity(entityList, camera);
        shadowMapMasterRenderer.render(entities);
        entities.clear();
    }

    public static void enableCulling() {
        GL11.glEnable(GL11.GL_CULL_FACE);
        GL11.glCullFace(GL11.GL_BACK);
    }

    public static void disableCulling() {
        GL11.glDisable(GL11.GL_CULL_FACE);
    }

    public void cleanup() {
        entityShader.cleanUp();
        terrainShader.cleanUp();
        guiShader.cleanUp();
        waterShader.cleanUp();
        skyBoxShader.cleanUp();
        normalMappingRenderer.cleanUp();
        ParticleMaster.cleanUp();
        shadowMapMasterRenderer.cleanUp();
    }

    private boolean checkDistance(Vector3f cameraPosition, Vector3f position) {
        float deltaX = cameraPosition.x - position.x;
        float deltaY = cameraPosition.y - position.y;
        float deltaZ = cameraPosition.z - position.z;

        float distance = (float) sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
        if (distance > Constants.RENDER_DISTANCE) return false;
        return true;
    }

    private float sqrt(float x) {
        if (x == 0) x = .1f;
        if (x == 1) x = 1.1f;
        int i;
        float s;
        s=((x/2)+x/(x/2)) / 2;
        for(i=1;i<=4;i++) {
            s=(s+x/s)/2;
        }
        return s;
    }

}
