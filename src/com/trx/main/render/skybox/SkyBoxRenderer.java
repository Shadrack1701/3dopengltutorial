package com.trx.main.render.skybox;

import com.trx.main.entities.Light;
import com.trx.main.render.model.RawModel;
import com.trx.main.utils.Loader;
import com.trx.main.utils.Maths;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public class SkyBoxRenderer {

    private static float blendFactor = 0;
    private static boolean day = true;
    private static float dayCounter = 0;
    private static String[] DAY_TEXTURE_FILES = {"SBright", "SBleft", "SBtop", "SBbottom", "SBback", "SBfront"};
    private static String[] NIGHT_TEXTURE_FILES = {"SBnightright", "SBnightleft", "SBnighttop", "SBnightbottom", "SBnightback", "SBnightfront"};
    private static Vector3f rotation = new Vector3f(0,0,0);
    private static float skyColor =.6f;
    private static Light sun = new Light(new Vector3f(200000, 300000, 200000),new Vector3f(1, 1, 1));

    private static final float SIZE = 500f;
    private static final float[] VERTICES = {
            -SIZE,  SIZE, -SIZE,
            -SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE,  SIZE, -SIZE,
            -SIZE,  SIZE, -SIZE,

            -SIZE, -SIZE,  SIZE,
            -SIZE, -SIZE, -SIZE,
            -SIZE,  SIZE, -SIZE,
            -SIZE,  SIZE, -SIZE,
            -SIZE,  SIZE,  SIZE,
            -SIZE, -SIZE,  SIZE,

            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE,  SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,

            -SIZE, -SIZE,  SIZE,
            -SIZE,  SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE, -SIZE,  SIZE,
            -SIZE, -SIZE,  SIZE,

            -SIZE,  SIZE, -SIZE,
            SIZE,  SIZE, -SIZE,
            SIZE,  SIZE,  SIZE,
            SIZE,  SIZE,  SIZE,
            -SIZE,  SIZE,  SIZE,
            -SIZE,  SIZE, -SIZE,

            -SIZE, -SIZE, -SIZE,
            -SIZE, -SIZE,  SIZE,
            SIZE, -SIZE, -SIZE,
            SIZE, -SIZE, -SIZE,
            -SIZE, -SIZE,  SIZE,
            SIZE, -SIZE,  SIZE
    };

    private RawModel cube;
    private int dayTexture;
    private int nightTexture;
    private SkyBoxShader shader;

    public SkyBoxRenderer(Loader loader, SkyBoxShader shader) {
        cube = loader.loadToVAO(VERTICES, 3);
        dayTexture = loader.loadCubeMap(DAY_TEXTURE_FILES);
        nightTexture = loader.loadCubeMap(NIGHT_TEXTURE_FILES);
        this.shader = shader;
        shader.start();
        shader.connectTextureUnits();
        Matrix4f matrix = Maths.createProjectionMatrix();
        shader.loadProjectionMatrix(matrix);
        shader.stop();
    }

    public void render() {
        GL30.glBindVertexArray(cube.getVaoID());
        GL20.glEnableVertexAttribArray(0);
        bindTextures();
        Matrix4f transformationMatrix = Maths.createTransformationMatrix(new Vector3f(0,0,0), rotation, 1);
        shader.loadTransformationMatrix(transformationMatrix);
        GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, cube.getVertexCount());
        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);
    }

    private void bindTextures() {
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, dayTexture);
        GL13.glActiveTexture(GL13.GL_TEXTURE1);
        GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, nightTexture);
        shader.loadBlendFactor(blendFactor);
    }

    public static void rotate() {
        SkyBoxRenderer.rotation.y = rotation.y + 0.01f;
        if (SkyBoxRenderer.rotation.y == 360) SkyBoxRenderer.rotation.y = 0;
    }

    public static void dayNight() {
        dayCounter++;
        if (day && dayCounter < 10000) return;
        if (!day && dayCounter < 6666) return;
        if (blendFactor <= 1 && day) {
            if (!day) {
                day = true;
                sun.setColour(new Vector3f(1,1,1));
                skyColor =.6f;
            }
            blendFactor = blendFactor + .0004f;
            if (skyColor > .0003) skyColor = skyColor - .0004f;
            sun.setColour(new Vector3f(sun.getColour().getX() - .0004f, sun.getColour().getY() - .0004f, sun.getColour().getZ() - .0004f));

            day = true;
        } else {
            if (day) {
                day = false;
                sun.setColour(new Vector3f(0.2f, 0.2f, 0.2f));
                skyColor =0;
                dayCounter = 0;
            } else {
                blendFactor = blendFactor - .0008f;
                if (skyColor <=.54f) skyColor =skyColor + .0006f;
                if (blendFactor <= 0) day = true;
                sun.setColour(new Vector3f(sun.getColour().getX() + .0008f, sun.getColour().getY() + .0008f, sun.getColour().getZ() + .0008f));
            }
        }
    }

    public static boolean isDay() {
        return day;
    }

    public static float getDayCounter() {
        return dayCounter;
    }

    public static float getBlendFactor() {
        return blendFactor;
    }

    public static Vector3f getSkyColor() {
        return new Vector3f(skyColor, skyColor, skyColor);
    }

    public static Light getSun() {
        return sun;
    }
}

