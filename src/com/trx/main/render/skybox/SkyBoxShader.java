package com.trx.main.render.skybox;

import com.trx.main.entities.Camera;
import com.trx.main.render.shader.ShaderProgram;
import com.trx.main.utils.Maths;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public class SkyBoxShader extends ShaderProgram {

    private static final String VERTEX_FILE = "res/shaders/skyBoxVertexShader.txt";
    private static final String FRAGMENT_FILE = "res/shaders/skyBoxFragmentShader.txt";

    private int location_transformationMatrix;
    private int location_projectionMatrix;
    private int location_viewMatrix;
    private int location_fogColor;
    private int location_cubeMapDay;
    private int location_cubeMapNight;
    private int location_blendFactor;

    public SkyBoxShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    public void loadTransformationMatrix(Matrix4f matrix){
        super.loadMatrix(location_transformationMatrix, matrix);
    }

    public void loadProjectionMatrix(Matrix4f projection){
        super.loadMatrix(location_projectionMatrix, projection);
    }

    public void loadViewMatrix(Camera camera){
        Matrix4f viewMatrix = Maths.createViewMatrix(camera);
        viewMatrix.m30 = 0;
        viewMatrix.m31 = 0;
        viewMatrix.m32 = 0;
        super.loadMatrix(location_viewMatrix, viewMatrix);
    }

    public void loadFogColor(Vector3f fogColor) {
        super.loadVector(location_fogColor, fogColor);
    }

    public void loadBlendFactor(float blendFactor) {
        super.loadFloat(location_blendFactor, blendFactor);
    }

    public void connectTextureUnits() {
        super.loadInt(location_cubeMapDay, 0);
        super.loadInt(location_cubeMapNight, 1);
    }

    @Override
    protected void getAllUniformLocations() {
        location_transformationMatrix = super.getUniformLocation("transformationMatrix");
        location_projectionMatrix = super.getUniformLocation("projectionMatrix");
        location_viewMatrix = super.getUniformLocation("viewMatrix");
        location_fogColor = super.getUniformLocation("fogColor");
        location_cubeMapDay = super.getUniformLocation("cubeMapDay");
        location_cubeMapNight = super.getUniformLocation("cubeMapNight");
        location_blendFactor = super.getUniformLocation("blendFactor");
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
    }

}
